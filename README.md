# telkabeh

telkabeh is a tools for test ping and checking specific port on multiple hosts. This tools written in Python 2.6 and can be execute without installing

## Requirement:
- Python 2.x

## Usage:
```
telkabeh.py [-h HOST | -f FILE] [-p PORT]

optional arguments:
  -h HOST, --host HOST  Hostname, fqdn or IP of servers
  -f FILE, --file FILE  File that contain list of the IP of servers
  -p PORT, --port PORT  Specifies port number that want to scan
```

telkabeh use PING method to check the availability of server. With the additional feature that also could checking the specific port of the server. If command run with `-p` or `--port` option, it will checking all the IPs listed based on PING method before check the port that you provide. Else, it just check the availability.

For default, telkabeh need the file which contain the list of IPs. For sure, you can check the example named `listip.txt` to see the format of file. Simply is just a bunch of IPs which separated by new line. But in some case, you can also check single IP with arguments `-h` or `--host`.

Before use this tools make sure the file are executable, or change it by
```
$ chmod +x telkabeh.py
```

## Examples:
For checking single IP on port 22:
```
$ ./telkabeh.py -h 1.2.3.4 -p 22
```
For checking all IPs listed on listip.txt on port 22:
```
$ ./telkabeh.py -f listip.txt -p 22
```

For checking availability of all IPs listed on listip.txt:
```
$ ./telkabeh.py -f listip.txt
```

## Tips:

You also can create a symbolic link at `/usr/local/bin` for flexibilty so you can run this command from every path.
```
sudo ln -s /usr/local/bin/telkabeh /path/of/file/telkabeh.py
```
and execute the tools every where with just type `telkabeh`