#!/usr/bin/python
# @author      : Delta Sierra (gitlab.com/dsubariyanto)
# @version     : 1.0
# @description : Tools for test ping and checking specific port on multiple hosts

import os
import subprocess
import sys
import socket
import argparse

def main():
    parser = argparse.ArgumentParser(add_help=False)
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-h', '--host', help='Hostname, fqdn or IP of servers')
    group.add_argument('-f', '--file', help='File that contain list of the IP of servers')
    parser.add_argument('-p', '--port', type=int, help='Specifies port number that want to scan')
    args = parser.parse_args()

    if args.host:
        shost = args.host
        if args.port:
            sport = args.port
            check_port(shost,sport)
        else:
            print '====== Checking connection: ======'
            just_ping(shost)
    elif args.file:
        files = args.file
        if args.port:
            port = args.port
            with open(files) as f:
                for line in f:
                    h = line.strip()
                    hosts_port[str(h)] = int(port)
            check_all_port()
        else:
            print '====== Checking connection: ======'
            with open(files) as f:
                for line in f:
                    h = line.strip()
                    just_ping(h)
    else:
        parser.print_help()

def just_ping(h):
    status = check_ping(h)
    if status:
        print "Pinging to " + h.upper() + ": " + '\033[1;32mREACHED\033[0m'
    else:
        print "Pinging to " + h.upper() + ": " + '\033[1;31mNOT REACHED\033[0m'

def check_all_port():
    print '====== Checking connection: ======'
    for h, i in sorted(hosts_port.items()):
        status = check_ping(h)
        if status:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            result = sock.connect_ex((h,i))
            if result == 0:
                print h.upper() + " on port " + str(i) + ": " + '\033[1;32mCONNECTED\033[0m'
            else:
                print h.upper() + " on port " + str(i) + ": " + '\033[1;31mNOT CONNECTED\033[0m'
            sock.close()
        else:
            print h.upper() + " on port " + str(i) + ": " + '\033[1;31mHOST MAY BE DOWN\033[0m'
    print ''

def check_port(shost,sport):
    status = check_ping(shost)
    if status:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex((shost,sport))
        if result == 0:
            print shost.upper() + " on port " + str(sport) + ": " + '\033[1;32mCONNECTED\033[0m'
        else:
            print shost.upper() + " on port " + str(sport) + ": " + '\033[1;31mNOT CONNECTED\033[0m'
        sock.close()
    else:
        print shost.upper() + " on port " + str(sport) + ": " + '\033[1;31mHOST MAY BE DOWN\033[0m'
        sys.exit(1)

def check_ping(pinghost):
    with open(os.devnull, 'w') as DEVNULL:
        try:
            subprocess.check_call(
                ['ping', '-c', '1', pinghost],
                stdout=DEVNULL,  # suppress output
                stderr=DEVNULL
            )
            is_up = True
        except subprocess.CalledProcessError:
            is_up = False
        return is_up

if __name__ == "__main__":
    hosts_port = {}
    main()
